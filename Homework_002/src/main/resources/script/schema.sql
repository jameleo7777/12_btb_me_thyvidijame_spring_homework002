CREATE DATABASE Mart;

CREATE TABLE product_tb(
    product_id SERIAL4 PRIMARY KEY,
    product_name VARCHAR(255),
    product_price REAL
);

INSERT INTO product_tb VALUES (DEFAULT,'Apple',0.25),
                              (DEFAULT,'Orange',0.30),
                              (DEFAULT,'Watermelon',0.50),
                              (DEFAULT,'PineApple',0.30),
                              (DEFAULT,'Grape',2);

CREATE TABLE IF NOT EXISTS customer_tb(
    customer_id SERIAL4 PRIMARY KEY ,
    customer_name VARCHAR(40),
    customer_address VARCHAR(50),
    customer_phone VARCHAR(10)
);
-- Default Data in customer_tb
INSERT INTO customer_tb VALUES (Default,'Vidijame','Chak Angre','098498403'),
                               (Default,'Socheat','Toul Kork','087430634'),
                               (Default,'HouyHyong','Phnum Penh','012369718'),
                               (Default,'Salin','Tek Tla','012293876'),
                               (Default,'Savong','Kandal','089765432');

CREATE TABLE invoice_tb(
    invoice_id SERIAL4 PRIMARY KEY,
    invoice_date TIMESTAMP,
    customer_id INTEGER REFERENCES customer_tb(customer_id) ON DELETE CASCADE
);

CREATE TABLE invoice_detail_tb(
    id SERIAL4 PRIMARY KEY,
    invoice_id INTEGER REFERENCES invoice_tb(invoice_id) ON UPDATE CASCADE ON DELETE CASCADE,
    product_id INTEGER REFERENCES product_tb(product_id) ON UPDATE CASCADE ON DELETE CASCADE
)