package com.example.homework_002.model.response;

import com.example.homework_002.model.entity.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse <T>{

    @JsonInclude(JsonInclude.Include. NON_NULL)
    private T payload;
    private String message;

    private Boolean success;


}
