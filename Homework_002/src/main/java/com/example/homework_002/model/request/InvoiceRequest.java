package com.example.homework_002.model.request;

import com.example.homework_002.model.entity.Invoice;
import com.example.homework_002.model.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class InvoiceRequest {
    private Timestamp invoice_date;
    private Integer customer_id;
    private  List<Integer> product_id;
}
