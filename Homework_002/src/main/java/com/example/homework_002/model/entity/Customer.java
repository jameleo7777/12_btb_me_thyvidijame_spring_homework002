package com.example.homework_002.model.entity;


import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Customer {
    private Integer customer_id;
    private String customer_name;
    private String customer_address;
    private String customer_phone;
}
