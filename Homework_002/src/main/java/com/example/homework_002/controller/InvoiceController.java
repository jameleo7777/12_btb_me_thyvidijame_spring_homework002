package com.example.homework_002.controller;


import com.example.homework_002.model.entity.Invoice;
import com.example.homework_002.model.request.InvoiceRequest;
import com.example.homework_002.model.response.InvoiceResponse;
import com.example.homework_002.repository.InvoiceRepository;
import com.example.homework_002.service.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/get-all-invoice")
    @Operation(summary = "Get All Invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse invoiceResponse = InvoiceResponse.<List<Invoice>>builder()
                .payload(invoiceService.getAllInvoice())
                .message("You have fetched all invoices successfully !!!")
                .success(true)
                .build();
        return ResponseEntity.ok(invoiceResponse);
    }

    @GetMapping("/get-invoice-by-id{id}")
    @Operation(summary = "Get Invoice By Id")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer id){
        InvoiceResponse invoiceResponse = null;
        if(invoiceService.getInvoiceById(id) != null){
            invoiceResponse = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(id))
                    .message("You have found this invoice successfully !!!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(invoiceResponse);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/delete-invoice-by-id{id}")
    @Operation(summary = "Delete Invoice By Id")
    public ResponseEntity<InvoiceResponse <String>> deleteInvoiceById(@PathVariable("id") Integer id){
        InvoiceResponse invoiceResponse = null;
        if(invoiceService.deleteInvoiceById(id) == true){
            invoiceResponse = InvoiceResponse.<String>builder()
                    .message("You have deleted this invoice successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(invoiceResponse);
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/add-new-invoice")
    @Operation(summary = "Add New Invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        InvoiceResponse invoiceResponse = null;
        Integer invoiceId = invoiceService.addInvoice(invoiceRequest);

        if(invoiceId != null){
            invoiceResponse = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .message("You have added new invoice successfully !!!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(invoiceResponse);
        }
        return null;
    }

    @PutMapping("/update-invoice-by-id{id}")
    @Operation(summary = "Update Invoice By Id")
    public ResponseEntity<InvoiceResponse<Invoice>> updateInvoice(@RequestBody InvoiceRequest invoiceRequest, @PathVariable("id") Integer id){
        InvoiceResponse invoiceResponse = null;
        Integer storeId = invoiceService.updateInvoice(invoiceRequest, id);

        if(storeId != null){
            invoiceResponse = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeId))
                    .message("Update Successful")
                    .success(true)
                    .build();
            return ResponseEntity.ok(invoiceResponse);
        }
        return null;
    }





}
