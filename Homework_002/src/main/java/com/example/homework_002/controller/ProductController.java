package com.example.homework_002.controller;


import com.example.homework_002.model.entity.Invoice;
import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.model.response.ProductResponse;
import com.example.homework_002.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.websocket.OnError;
import org.apache.logging.log4j.util.PropertiesUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    // Injection
    private final ProductService productService;


    // Injection By Constructor
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    // Show All Product
    @GetMapping("/get-all-product")
    @Operation(summary = "Show All Product")
    public ResponseEntity<ProductResponse<List<Product>>>getAllProduct(){

        ProductResponse productResponse = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProduct())
                .message("You have fetched all products successfully !!!")
                .success(true)
                .build();
        return ResponseEntity.ok(productResponse);
    }

    // Show Product By ID
    @GetMapping("/get-product-by-id{id}")
    @Operation(summary = "Show Product By ID")
    public ResponseEntity<ProductResponse<Product>>getProductById(@PathVariable Integer id){
        ProductResponse productResponse = null;
        if(productService.getProductById(id) != null){
           productResponse  = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(id))
                    .message("You have found this product successfully !!!")
                    .success(true)
                    .build();
           return ResponseEntity.ok(productResponse);

        }
        else{
            productResponse = ProductResponse.<Product>builder()
                    .message("You cannot find this product, product does not exist !!!")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(productResponse);
        }


    }

    // Delete Product By ID

    @DeleteMapping("/delete-product-by-id{id}")
    @Operation(summary = "Delete Product By ID")
    public ResponseEntity<ProductResponse<String>>deleteProductById(@PathVariable("id") Integer id){
        ProductResponse productResponse = null;
        if(productService.deleteProductById(id) == true){
            productResponse = ProductResponse.<String>builder().
                    message("You have deleted this product successfully !!!").
                    success(true).
                    build();
            return ResponseEntity.ok(productResponse);
        }else{
            productResponse = ProductResponse.<String>builder()
                    .message("You cannot delete this product, product does not exist !!!")
                    .success(false)
                    .build();

            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(productResponse);
        }


    }

    // Add New Product
    @PostMapping("/add-new-product")
    @Operation(summary = "Add New Product")
    public ResponseEntity<ProductResponse<Product>> addProduct(@RequestBody ProductRequest productRequest){
        Integer productID = productService.addProduct(productRequest);
        if(productID != null){
            ProductResponse productResponse = ProductResponse.<Product>builder().
                    payload(productService.getProductById(productID)).
                    message("You have added product successfully !!!").
                    success(true).
                    build();
            return ResponseEntity.ok(productResponse);

        }
        return null;
    }

    // Update Product
    @PutMapping("/update-product-by-id{id}")
    @Operation(summary = "Update Product By ID")
    public ResponseEntity<ProductResponse<Product>> updateProduct(@RequestBody ProductRequest productRequest, @PathVariable("id") Integer productID){
        ProductResponse productResponse = null;
        Integer updateID = productService.updateProduct(productRequest, productID);
        if(updateID != null){
            productResponse = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(updateID))
                    .message("You have updated this product successfully !!!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(productResponse);
        } else {
            productResponse = ProductResponse.<Product>builder()
                    .message("Cannot Update, This product have not exist in stock !!!")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(productResponse);
        }

    }

}
