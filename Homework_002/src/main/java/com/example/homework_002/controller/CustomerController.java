package com.example.homework_002.controller;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;
import com.example.homework_002.model.response.CustomerResponse;
import com.example.homework_002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.apache.catalina.util.CustomObjectInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    @Operation(summary = "Show All Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){
        CustomerResponse customerResponse = CustomerResponse.<List<Customer>>builder()
                .payload(customerService.showAllCustomer())
                .message("You have fetched all customers successfully !!!")
                .success(true)
                .build();
        return ResponseEntity.ok(customerResponse);
    }

    @GetMapping("/get-customer-by-id{id}")
    @Operation(summary = "Get Customer By ID")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer id){
        CustomerResponse customerResponse = null;
        System.out.println(id);
        if(customerService.getCustomerById(id) != null){
            customerResponse = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(id))
                    .message("You have found customer successfully")
                    .success(true)
                    .build();
            return ResponseEntity.ok(customerResponse);
        }else{
            customerResponse = CustomerResponse.<Customer>builder()
                    .message("Cannot find, Customer does not exist !!!")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(customerResponse);
        }
    }

    @PutMapping("/update-customer-by-id{id}")
    @Operation(summary = "Update Customer By ID")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(@RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer customerID){
        CustomerResponse customerResponse = null;
        Integer updateCustomer = customerService.updateCustomerById(customerRequest,customerID);
        if(updateCustomer != null){
            customerResponse = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(updateCustomer))
                    .message("You have updated this customer successfully !!!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(customerResponse);
        }else {
            customerResponse = CustomerResponse.<Customer>builder()
                    .message("Cannot update this customer, custoemr not exist")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(customerResponse);
        }
    }

    @PostMapping("/add-new-customer")
    @Operation(summary = "Add New Customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        CustomerResponse customerResponse = null;
        Integer customerID = customerService.addCustomer(customerRequest);
        if(customerID != null){
            customerResponse = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(customerID))
                    .message("You have added customer successfully !!!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(customerResponse);

        }
        return null;
    }

    @DeleteMapping("/delete-customer-by-id{id}")
    @Operation(summary = "Delete Customer By ID")
    public ResponseEntity<CustomerResponse<String>>deleteCustomerById(@PathVariable("id") Integer id){
        CustomerResponse customerResponse = null;
        if(customerService.deleteCustomerById(id) == true){
            customerResponse = CustomerResponse.<String>builder()
                    .message("You have deleted this customer successfully !!!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(customerResponse);
        }else {
            customerResponse = CustomerResponse.<String>builder()
                    .message("You cannot delete this customer, customer is not exist !!!")
                    .success(false)
                    .build();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(customerResponse);
        }
    }




}
