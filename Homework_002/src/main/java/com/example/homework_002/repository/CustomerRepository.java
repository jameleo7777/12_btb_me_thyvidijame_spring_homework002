package com.example.homework_002.repository;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;
import com.example.homework_002.model.request.ProductRequest;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Mapper

public interface CustomerRepository {

    @Select("SELECT * FROM customer_tb")

    List<Customer> showAllCustomer();

    @Select("SELECT * FROM customer_tb WHERE customer_id = #{id}")
    Customer getCustomerById(Integer id);

    @Select("UPDATE customer_tb "+
            "SET customer_name = #{updateCustomer.customer_name}, "+
            "customer_address = #{updateCustomer.customer_address},  "+
            "customer_phone = #{updateCustomer.customer_phone} \n" +
            "WHERE customer_id = #{customerID} \n"+
            "RETURNING customer_id ")
    Integer updateCustomer(@Param("updateCustomer") CustomerRequest customerRequest, Integer customerID);

    @Select("INSERT INTO customer_tb (customer_name, customer_address, customer_phone) VALUES(#{add.customer_name}, #{add.customer_address}, #{add.customer_phone}) RETURNING customer_id")
    Integer addCustomer(@Param("add") CustomerRequest customerRequest);

    @Delete("DELETE FROM customer_tb WHERE customer_id = #{id}")
    boolean deleteCustomerById(Integer id);
}
