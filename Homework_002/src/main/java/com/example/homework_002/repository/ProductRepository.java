package com.example.homework_002.repository;


import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.model.response.ProductResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("SELECT * FROM product_tb ORDER BY product_id")
    List<Product> showAllProduct();

    @Select("SELECT * FROM product_tb WHERE product_id = #{id}")
    Product getProductById(Integer id);

    @Select("SELECT p.product_id, p.product_name, p.product_price FROM invoice_detail_tb iv " +
            " INNER JOIN product_tb p ON p.product_id = iv.product_id " +
            " WHERE iv.invoice_id  = #{id}")
    List<Product> getProductByInvoiceId(Integer id);

    @Delete("DELETE FROM product_tb WHERE product_id = #{id}")
    boolean deleteProductById(Integer id);

    @Select("INSERT INTO product_tb (product_name,product_price) VALUES(#{add.product_name}, #{add.product_price}) RETURNING product_id")
    Integer addProduct(@Param("add")ProductRequest productRequest);

    @Select("UPDATE product_tb "+
            "SET product_name = #{update.product_name},"+
            "product_price = #{update.product_price} "+
            "WHERE product_id = #{productID} "+
            "RETURNING product_id")
    Integer updateProduct(@Param("update")ProductRequest productRequest, Integer productID);


}
