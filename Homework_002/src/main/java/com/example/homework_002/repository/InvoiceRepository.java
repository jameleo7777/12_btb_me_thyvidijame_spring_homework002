package com.example.homework_002.repository;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.entity.Invoice;
import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.InvoiceRequest;
import com.example.homework_002.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("SELECT * FROM invoice_tb")
    @Results(
            id = "InvoiceMapper",
            value = {
                    @Result(property = "customer", column = "customer_id"
                            ,one = @One(select = "com.example.homework_002.repository.CustomerRepository.getCustomerById")
                    ),
                    @Result(property = "invoice_id", column = "invoice_id"),
                    @Result(property = "products", column = "invoice_id", many = @Many(select = "com.example.homework_002.repository.ProductRepository.getProductByInvoiceId"))
            }
    )

    List<Invoice> findAllInvoice();

    @Select("SELECT * FROM invoice_tb WHERE invoice_id = #{id}")
    @ResultMap("InvoiceMapper")
    Invoice getInvoiceById(Integer id);

    @Delete("DELETE FROM invoice_tb WHERE invoice_id = #{id}")
    @ResultMap("InvoiceMapper")
    boolean deleteInvoiceById(Integer id);




    @Select("INSERT INTO invoice_tb (invoice_date, customer_id ) " +
            "VALUES(#{add.invoice_date}, #{add.customer_id}) RETURNING invoice_id")
    Integer addInvoice(@Param("add") InvoiceRequest invoiceRequest);

    @Insert("INSERT INTO invoice_detail_tb (invoice_id, product_id) " +
            "VALUES (#{invoice_id}, #{product_id})")
    Integer saveInvoiceDetail( Integer invoice_id, Integer product_id);


    @Select("UPDATE invoice_tb "+
            "SET invoice_date = #{update.invoice_date},"+
            "customer_id = #{update.customer_id} "+
            "WHERE invoice_id = #{invoice_id} "+
            "RETURNING invoice_id")
    Integer updateProduct(@Param("update") InvoiceRequest invoiceRequest, Integer invoice_id);

    @Delete(" DELETE FROM invoice_detail_tb WHERE invoice_id = #{invoice_id}")
    boolean delete(Integer invoice_id);





}
