package com.example.homework_002;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Mart REST API", description = "Welcome to Spring REST API", version = "v1"))
public class Homework002Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework002Application.class, args);
    }

}
