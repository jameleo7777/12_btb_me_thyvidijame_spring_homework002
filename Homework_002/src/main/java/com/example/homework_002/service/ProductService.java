package com.example.homework_002.service;

import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.model.response.ProductResponse;
import org.apache.ibatis.ognl.BooleanExpression;

import java.util.List;

public interface ProductService {
    List<Product> getAllProduct();

    Product getProductById(Integer id);

    boolean deleteProductById(Integer id);

    Integer addProduct(ProductRequest productRequest);

    Integer updateProduct(ProductRequest productRequest, Integer productID);
}
