package com.example.homework_002.service.ProductServiceImp;

import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.repository.ProductRepository;
import com.example.homework_002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductServiceImp implements ProductService {


    private final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.showAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public boolean deleteProductById(Integer id) {
        return productRepository.deleteProductById(id);
    }

    @Override
    public Integer addProduct(ProductRequest productRequest) {
        Integer productId = productRepository.addProduct(productRequest);
        return productId;
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productID) {
        return productRepository.updateProduct(productRequest, productID);
    }
}
