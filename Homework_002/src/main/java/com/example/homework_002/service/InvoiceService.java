package com.example.homework_002.service;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.entity.Invoice;
import com.example.homework_002.model.request.InvoiceRequest;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.repository.InvoiceRepository;

import java.util.List;

public interface InvoiceService {

    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer id);

    boolean deleteInvoiceById(Integer id);

    Integer addInvoice(InvoiceRequest invoiceRequest);

    Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoice_id);









}
