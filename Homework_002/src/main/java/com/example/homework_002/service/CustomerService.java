package com.example.homework_002.service;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    List<Customer> showAllCustomer();

    Customer getCustomerById(Integer id);

    Integer updateCustomerById(CustomerRequest customerRequest, Integer id);

    Integer addCustomer(CustomerRequest customerRequest);

    boolean deleteCustomerById(Integer id);

}
