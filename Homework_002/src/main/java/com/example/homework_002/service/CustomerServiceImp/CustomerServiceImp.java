package com.example.homework_002.service.CustomerServiceImp;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;
import com.example.homework_002.repository.CustomerRepository;
import com.example.homework_002.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CustomerServiceImp implements CustomerService {

    private final CustomerRepository customerRepository;


    @Autowired
    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Customer> showAllCustomer() {
        System.out.println(customerRepository.showAllCustomer());
        return customerRepository.showAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Integer updateCustomerById(CustomerRequest customerRequest, Integer customerID) {
        return customerRepository.updateCustomer(customerRequest, customerID);
    }

    @Override
    public Integer addCustomer(CustomerRequest customerRequest) {
        Integer customerId = customerRepository.addCustomer(customerRequest);
        return customerId;
    }

    @Override
    public boolean deleteCustomerById(Integer id) {
        return customerRepository.deleteCustomerById(id);
    }
}
