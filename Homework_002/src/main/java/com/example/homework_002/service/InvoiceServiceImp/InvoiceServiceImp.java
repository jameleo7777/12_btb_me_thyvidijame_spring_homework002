package com.example.homework_002.service.InvoiceServiceImp;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.entity.Invoice;
import com.example.homework_002.model.request.InvoiceRequest;
import com.example.homework_002.repository.InvoiceRepository;
import com.example.homework_002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public boolean deleteInvoiceById(Integer id) {
        return invoiceRepository.deleteInvoiceById(id);
    }

    @Override
    public Integer addInvoice(InvoiceRequest invoiceRequest) {
        Integer storeInvoiceId = invoiceRepository.addInvoice(invoiceRequest);

        for(Integer invoiceId : invoiceRequest.getProduct_id()){
            invoiceRepository.saveInvoiceDetail(storeInvoiceId,invoiceId);
        }

        return storeInvoiceId;

    }

    @Override
    public Integer updateInvoice(InvoiceRequest invoiceRequest, Integer invoice_id) {
        Integer storeInvoiceId = invoiceRepository.updateProduct(invoiceRequest, invoice_id);

        invoiceRepository.delete(invoice_id);
        for(Integer invoiceId : invoiceRequest.getProduct_id()){
            invoiceRepository.saveInvoiceDetail(storeInvoiceId,invoiceId);
        }
        return storeInvoiceId;
    }


}
